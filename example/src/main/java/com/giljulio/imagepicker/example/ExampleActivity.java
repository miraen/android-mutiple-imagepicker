package com.giljulio.imagepicker.example;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.giljulio.imagepicker.ui.ImagePickerActivity;
import com.giljulio.imagepicker.utils.ImageInternalFetcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ExampleActivity extends Activity {

    private static final String TAG = ExampleActivity.class.getSimpleName();
    private static final int RESULT_IMAGE_PICKER = 9000;

    private static final String picturePath = "/storage/emulated/0/Pictures/KakaoTalk/1415763746139.jpeg";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.example, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        private TextView mActivityResultsTextView;
        public ImageInternalFetcher mImageFetcher;
        private ImageView iv,iv1,iv2,iv3,iv4;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_example, container, false);


            mActivityResultsTextView = (TextView) rootView.findViewById(R.id.result);
//
            Button button = (Button) rootView.findViewById(R.id.pick_images);
            button.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
                    startActivityForResult(intent, RESULT_IMAGE_PICKER);
                }
            });
            mImageFetcher = new ImageInternalFetcher(rootView.getContext(), 500);
            iv = (ImageView) rootView.findViewById(R.id.imageView1);
            iv1 = (ImageView) rootView.findViewById(R.id.imageView2);
            iv2 = (ImageView) rootView.findViewById(R.id.imageView3);
            iv3 = (ImageView) rootView.findViewById(R.id.imageView4);
            iv4 = (ImageView) rootView.findViewById(R.id.imageView5);

            return rootView;
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            Log.d(TAG, "RequestCode: " + requestCode + "  ResultCode: " + resultCode);
            switch (requestCode) {
                case RESULT_IMAGE_PICKER:
                    if (resultCode == Activity.RESULT_OK) {
                        Parcelable[] parcelableUris = data.getParcelableArrayExtra(ImagePickerActivity.TAG_IMAGE_URI);
                        Uri[] uris = new Uri[parcelableUris.length];
                        System.arraycopy(parcelableUris, 0, uris, 0, parcelableUris.length);
                        int count = 0;
                        for (Uri uri : uris) {
                            Log.d(TAG,uri+":");
                            Log.d(TAG, count+"<-카운트" + uri.getPath());
                          // mActivityResultsTextView.setText(mActivityResultsTextView.getText() + " " + uri.getPath());
                            switch (count) {
                                case 0:
                                    iv.setVisibility(View.VISIBLE);
                                    iv.setImageBitmap(imageRendering(uri));
                                    break;
                                case 1:
                                    iv1.setVisibility(View.VISIBLE);
                                    iv1.setImageBitmap(imageRendering(uri));
                                    break;
                                case 2:
                                    iv2.setVisibility(View.VISIBLE);
                                    iv2.setImageBitmap(imageRendering(uri));
                                    break;
                                case 3:
                                    iv3.setVisibility(View.VISIBLE);
                                    iv3.setImageBitmap(imageRendering(uri));

                                    break;
                                case 4:
                                    iv4.setVisibility(View.VISIBLE);
                                    iv4.setImageBitmap(imageRendering(uri));

                                    break;

                            }    count++;



                        }
                    }
                    break;

                default:
                    super.onActivityResult(requestCode, resultCode, data);
                    break;
            }
        }

        public Bitmap imageRendering(Uri uri) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inInputShareable = true;
            options.inDither = false;
            options.inTempStorage = new byte[32 * 1024];
            options.inPurgeable = true;
            options.inJustDecodeBounds = false;

            File f = new File(uri.getPath());

            FileInputStream fs = null;
            try {
                fs = new FileInputStream(f);
            } catch (FileNotFoundException e) {
                //TODO do something intelligent
                e.printStackTrace();
            }

            Bitmap bm = null;

            try {
                if (fs != null) bm = BitmapFactory.decodeFileDescriptor(fs.getFD(), null, options);
            } catch (IOException e) {
                //TODO do something intelligent
                e.printStackTrace();
            } finally {
                if (fs != null) {
                    try {
                        fs.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

            return bm;
        }//imageRendering close


    }


}
