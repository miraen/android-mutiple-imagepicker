package com.giljulio.imagepicker.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.giljulio.imagepicker.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class PictureControlActivity extends Activity {

    private ImageView mPickerImage;
    private Button mBtnRotate, mBtnDelete, mBtnUsePictre;
    private final Activity mActivity = PictureControlActivity.this;
    private static final int ROTATE_VALUE = 90;
    private int currentAngle = 0;
    private Bitmap mOrgImage;
    private Button mBtnRepresentative;
    private static class ViewHolder {

    }

    private ViewHolder holder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        holder = new ViewHolder();
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_picture_control);

        mBtnRotate = (Button) findViewById(R.id.control_btn_rotate);
       // holder.mBtnRepresentative = (Button) findViewById(R.id.control_btn_representative);
        mBtnDelete = (Button) findViewById(R.id.control_btn_delete);
        mBtnUsePictre = (Button) findViewById(R.id.control_btn_use_picture);
        mPickerImage = (ImageView) findViewById(R.id.imageView);
        mPickerImage.getLayoutParams().height = mActivity.getResources().getDisplayMetrics().widthPixels;
        mBtnRepresentative = (Button) findViewById(R.id.control_btn_representative);
        //TODO INTENT
        Intent intent = getIntent();
        Uri uri = intent.getParcelableExtra("image_container_uri");
        final ArrayList<Uri> parcel = intent.getParcelableArrayListExtra("image_container_uri_list");

        for (int i = 0; i < parcel.size(); i++) {
            Log.d("TAG", "get" + parcel.get(i));
        }
        //TODO
        mOrgImage = imageRendering(uri);
        mPickerImage.setImageBitmap(mOrgImage);
        mBtnRotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "해당사진이 회전되었습니다.", Toast.LENGTH_SHORT).show();
                int width = mPickerImage.getWidth();
                int height = mPickerImage.getHeight();
                currentAngle = ROTATE_VALUE + (currentAngle % 360);

                Bitmap mRotate = getImageRotete(mOrgImage, currentAngle);
                mPickerImage.setImageBitmap(mRotate);

            }
        });

        mBtnRepresentative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "대표사진으로 설정되었습니다.", Toast.LENGTH_SHORT).show();
                //TODO FIX Toggle버튼 만들기.

           //     holder.mBtnRepresentative.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_picker_representative_seleted, 0, 0);

                //parcel.add(0, Uri.parse("/storage/emulated/0/Pictures/Screenshots/Screenshot_qqqqq.png"));

                //TODO FIX parcel.get(1)과 parcel.get(2) 함수형 필요.
                parcel.add(0, parcel.get(1));
               if(parcel.get(2)!=null){
                   Log.d("TAG", "있다." );
                   parcel.remove(2);
               }
               // parcel.remove(parcel.get(2));

                for (int i = 0; i < parcel.size(); i++) {

                    Log.d("TAG", "get" + parcel.get(i));
                }
            }
        });

        mBtnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "삭제되었습니다.", Toast.LENGTH_SHORT).show();
                new AlertDialog.Builder(mActivity).setMessage("현재 사진을 삭제하시겠습니까?").setNegativeButton("취소", null).setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //TODO FIX arraylist remove
                        parcel.remove(0);
                        for (int i = 0; i < parcel.size(); i++) {
                            Log.d("TAG", "get" + parcel.get(i));
                        }
                        onBackPressed();
                        finish();
                    }
                }).show();
            }
        });

        mBtnUsePictre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
//        mBtnUsePictre.setOnClickListener(this);

        //  Log.d("TAG", s);
        // textView.setText(uri.toString());


    }


    public Bitmap imageRendering(Uri uri) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inInputShareable = true;
        options.inDither = false;
        options.inTempStorage = new byte[32 * 1024];
        options.inPurgeable = true;
        options.inJustDecodeBounds = false;

        File f = new File(uri.getPath());

        FileInputStream fs = null;
        try {
            fs = new FileInputStream(f);
        } catch (FileNotFoundException e) {
            //TODO do something intelligent
            e.printStackTrace();
        }

        Bitmap bm = null;

        try {
            if (fs != null) bm = BitmapFactory.decodeFileDescriptor(fs.getFD(), null, options);
        } catch (IOException e) {
            //TODO do something intelligent
            e.printStackTrace();
        } finally {
            if (fs != null) {
                try {
                    fs.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

        return bm;
    }


    /**
     * 이미지 관련 처리를 수행한다.
     * 회전을 시키며, 이미지를 화면에 맞게 줄인다.
     *
     * @author master
     * 2011. 3. 31.
     */


    private Bitmap getImageRotete(Bitmap bitmap, int nrotate) {
        Matrix matrix = new Matrix();

        matrix.postRotate(currentAngle);
        // matrix.setRotate(currentAngle, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);
        //matrix.setRotate(mDegree, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);
        // 이미지의 해상도를 줄인다.
        /*float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        matrix.postScale(scaleWidth, scaleHeight);*/
        try {
            Bitmap rotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            //  if (bitmap != rotated) {
            //    bitmap.recycle();
            bitmap = rotated;
            // }
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
        // View 사이즈에 맞게 이미지를 조절한다.
        // Bitmap resize = Bitmap.createScaledBitmap(rotateBitmap, viewW, viewH, true);
        return bitmap;
    }


}
